const config       = require('config');
const Database     = require('./db');
const argv         = require('minimist')(process.argv.slice(2));
const randomString = require('randomstring');

const db = new Database(config.get('mongo.connectionString'));

(async function () {
  try {
    await db.connect(config.get('mongo.name'), config.get('mongo.options'));
    console.log('Database connected');
  }
  catch (e) {
    console.error('Database connect error', e);
    return;
  }

  //create fake players collection if have application argument --createUsers {usersCount}
  if (typeof argv.createUsers === 'number' && argv.createUsers > 0) {
    try {
      console.log('Clear players collection...');
      await db.clearPlayers();
      const playersInOneInsert = 100;
      for (let i = 0; i < argv.createUsers; i += playersInOneInsert) {
        console.log(`Creating fake users: ${i}/${argv.createUsers}`);
        let players = [];
        for (let j = 0; j < playersInOneInsert && i + j < argv.createUsers; j++) {
          players[j] = {_id: i + j, name: randomString.generate()};
        }
        await db.addPlayers(players);
      }
    }
    catch (e) {
      console.error('Fake users creation error', e);
      return;
    }
    console.log('Fake users created');
  }
})();

module.exports = db;